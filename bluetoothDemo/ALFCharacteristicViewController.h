//
//  ALFCharacteristicViewController.h
//  bluetoothDemo
//
//  Created by allfake on 7/6/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "TKMultiSwitch.h"
#import "ALFSimpleInputModel.h"

@interface ALFCharacteristicViewController : UIViewController <CBPeripheralDelegate, CBCentralManagerDelegate, UIActionSheetDelegate>
{
    ALFSimpleInputModel *input;
    NSInteger dataOption;
}
@property (readwrite, nonatomic) CBCentralManager *manager;
@property (nonatomic, strong) IBOutlet UILabel *readDataLabel;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) CBService *service;
@property (nonatomic, strong) CBCharacteristic *characteristic;

@property (nonatomic,strong) IBOutlet UIView *multiswitchView;
@property (nonatomic,strong) TKMultiSwitch *multiswitch;

@end
