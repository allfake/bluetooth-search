//
//  ALFColorPickerViewController.m
//  bluetoothDemo
//
//  Created by allfake on 8/24/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFColorPickerViewController.h"

@interface ALFColorPickerViewController ()

@end

@implementation ALFColorPickerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.colorPickerView setColor:[UIColor blueColor]];
    
    [self.colorPickerView addTarget:self
                        action:@selector(changeColor:)
              forControlEvents:UIControlEventValueChanged];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    [self.peripheral setDelegate:self];
    [self.peripheral setNotifyValue:YES forCharacteristic:self.characteristic];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.manager setDelegate:nil];
}

- (void)changeColor:(HRColorPickerView *)colorPicker
{
    NSString *v = [self hexStringForColor:colorPicker.color];
    NSData* valData = [v dataUsingEncoding:NSUTF8StringEncoding];
    
    CBCharacteristicWriteType type;
    
    if ((self.characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) == CBCharacteristicPropertyWriteWithoutResponse) {
        
        type = CBCharacteristicWriteWithoutResponse;
        
    } else {
        
        type = CBCharacteristicWriteWithResponse;
        
    }
    
    [self.peripheral writeValue:valData forCharacteristic:self.characteristic type:type];
}

- (IBAction)off:(id)sender
{
    NSString *v = @"000000";
    NSData* valData = [v dataUsingEncoding:NSUTF8StringEncoding];
    
    CBCharacteristicWriteType type;
    
    if ((self.characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) == CBCharacteristicPropertyWriteWithoutResponse) {
        
        type = CBCharacteristicWriteWithoutResponse;
        
    } else {
        
        type = CBCharacteristicWriteWithResponse;
        
    }
    
    [self.peripheral writeValue:valData forCharacteristic:self.characteristic type:type];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    
}

- (NSString *)hexStringForColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    NSString *hexString=[NSString stringWithFormat:@"%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
    return hexString;
}


@end
