//
//  main.m
//  bluetoothDemo
//
//  Created by allfake on 6/14/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ALFAppDelegate class]));
    }
}
