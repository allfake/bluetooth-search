//
//  ALFPadViewController.m
//  bluetoothDemo
//
//  Created by allfake on 7/12/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFGamepadSettingViewController.h"

@interface ALFGamepadSettingViewController ()

@end

@implementation ALFGamepadSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    padModel = [[ALFPadModel alloc] init];
    [padModel loadPad];
    
    [self loadPadToTextFiled];
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.scrollView contentSizeToFit];
}

- (void)loadPadToTextFiled
{
    self.upButton.text = padModel.upButtonString;
    self.downButton.text = padModel.downButtonString;
    self.leftButton.text = padModel.leftButtonString;
    self.rightButton.text = padModel.rightButtonString;
    self.aButton.text = padModel.aButtonString;
    self.bButton.text = padModel.bButtonString;
    self.cButton.text = padModel.cButtonString;
    self.dButton.text = padModel.dButtonString;
}

- (IBAction)save:(id)sender
{
    padModel.upButtonString = self.upButton.text;
    padModel.downButtonString = self.downButton.text;
    padModel.leftButtonString = self.leftButton.text;
    padModel.rightButtonString = self.rightButton.text;
    padModel.aButtonString = self.aButton.text;
    padModel.bButtonString = self.bButton.text;
    padModel.cButtonString = self.cButton.text;
    padModel.dButtonString = self.dButton.text;
    
    [padModel savePad];
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
