//
//  ALFBluetoothDetailViewController.m
//  bluetoothDemo
//
//  Created by allfake on 7/6/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFBluetoothDetailViewController.h"
#import "ALFBlueToothSeviceTableViewCell.h"
#import "ALFCharacteristicViewController.h"

#define SECTION_KEY @"secionIndex"

@interface ALFBluetoothDetailViewController ()

@end

@implementation ALFBluetoothDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dataSource = [NSMutableDictionary new];
    
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    
    [self.peripheral setDelegate:self];
    [self.peripheral discoverServices:nil];
    
    if (!self.peripheral.name || [self.peripheral.name isEqualToString:@""]) {
        self.title = @"Unnamed";
    } else {
        self.title = self.peripheral.name;
    }
    
    self.blueToothUUID.text = self.peripheral.identifier.UUIDString;
    self.tableView.tableHeaderView = self.headerView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:selectedIndexPath animated:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.manager cancelPeripheralConnection:self.peripheral];
}


#pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if ([segue.identifier isEqualToString:@"characteristicSegueIndentifier"]) {
     
     
         ALFCharacteristicViewController *destinationViewController = (ALFCharacteristicViewController *)[segue destinationViewController];
         [destinationViewController setCharacteristic:[sender objectForKey:@"Characteristic"]];
         [destinationViewController setService:[sender objectForKey:@"Service"]];
         [destinationViewController setPeripheral:self.peripheral];
         
     }
 }

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBCentralManagerStatePoweredOn) {
        self.blueToothStatus.text = @"Connect";
    } else {
        self.blueToothStatus.text = @"Not connect";
    }
    [self.tableView reloadData];
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error
{
    for (CBService *aService in aPeripheral.services)
    {
        NSLog(@"Service found with UUID: %@", aService.UUID);
        
//        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"FFE0"]])
//        {
        [aPeripheral discoverCharacteristics:nil forService:aService];
//        }
    }
    
    
//    [dataSource setObject:aPeripheral.services forKey:SECTION_KEY];
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        NSLog(@"Characteristic %@", aChar.UUID);
        
        NSDictionary *sectionAndRow = [[NSDictionary alloc] initWithObjectsAndKeys:service, @"Service", service.characteristics, @"Characteristic", nil];
        
        [dataSource setObject:sectionAndRow forKey:service.UUID.UUIDString];
        
        
        /* Read body sensor location */
//        if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"FFE1"]])
//        {
//            [aPeripheral setNotifyValue:YES forCharacteristic:aChar];
//            
//            
//            //                [self.manager connectPeripheral:aPeripheral options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBConnectPeripheralOptionNotifyOnConnectionKey]];
//            
//            
//        }
        
    }
    
    [self.tableView reloadData];
}

#pragma mark - Tableview

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor colorWithRed:245/255.f green:253/255.f blue:255/255.f alpha:1.0f]];
    header.contentView.backgroundColor = [UIColor colorWithRed:95/255.f green:182/255.f blue:255/255.f alpha:1.0f];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[dataSource allKeys] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray * keys = [dataSource allKeys];
    
    if ([keys count] == 0) {
        return @"";
    }
    
    CBService *service = [[dataSource objectForKey:[keys objectAtIndex:section]] objectForKey:@"Service"];
    
    return [NSString stringWithFormat:@"%@", service.UUID.UUIDString];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray * keys = [dataSource allKeys];
    
    if ([keys count] == 0) {
        return 0;
    }
    
    return [[[dataSource objectForKey:[keys objectAtIndex:section]] objectForKey:@"Characteristic"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [tableView dequeueReusableCellWithIdentifier:ALFBlueToothSeviceTableViewCellIdentifier];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ALFBlueToothSeviceTableViewCell class])
                                              owner:self
                                            options:nil] lastObject];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(ALFBlueToothSeviceTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSArray * keys = [dataSource allKeys];
    CBCharacteristic *characteristic = [[[dataSource objectForKey:[keys objectAtIndex:indexPath.section]] objectForKey:@"Characteristic"] objectAtIndex:indexPath.row];
    
    if (!characteristic.UUID.UUIDString || [characteristic.UUID.UUIDString isEqualToString:@""]) {
        
        cell.title.text = @"Unnamed";
        
    } else {
        
        cell.title.text = characteristic.UUID.UUIDString;
        
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *keys = [dataSource allKeys];
    CBCharacteristic *characteristic = [[[dataSource objectForKey:[keys objectAtIndex:indexPath.section]] objectForKey:@"Characteristic"] objectAtIndex:indexPath.row];
    CBService *service = [[dataSource objectForKey:[keys objectAtIndex:indexPath.section]] objectForKey:@"Service"];

    
    NSDictionary *serviceAndCharacteristic = [[NSDictionary alloc] initWithObjectsAndKeys:service, @"Service", characteristic, @"Characteristic", nil];
    
    [self performSegueWithIdentifier:@"characteristicSegueIndentifier" sender:serviceAndCharacteristic];
}

@end
