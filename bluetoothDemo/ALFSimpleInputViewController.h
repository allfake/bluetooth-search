//
//  ALFSimpleInputViewController.h
//  bluetoothDemo
//
//  Created by allfake on 8/24/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "ALFSimpleInputModel.h"

@interface ALFSimpleInputViewController : UIViewController <CBPeripheralDelegate, CBCentralManagerDelegate>
{
    ALFSimpleInputModel *input;
}
@property (readwrite, nonatomic) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) CBCharacteristic *characteristic;

@property (nonatomic, strong) IBOutlet UITextField *inputTextFiled;
@end
