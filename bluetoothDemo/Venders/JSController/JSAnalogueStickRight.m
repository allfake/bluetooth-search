//
//  JSAnalogueStick.m
//  Controller
//
//  Created by James Addyman on 29/03/2013.
//  Copyright (c) 2013 James Addyman. All rights reserved.
//

#import "JSAnalogueStickRight.h"

#define RADIUS ([self bounds].size.width / 2)

@implementation JSAnalogueStickRight

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
	self.xValue = 0.0;
	self.yValue = 0.0;
	
	CGRect handleImageFrame = [self.handleImageView frame];
	handleImageFrame.origin = CGPointMake(([self bounds].size.width - [self.handleImageView bounds].size.width) / 2,
										  ([self bounds].size.height - [self.handleImageView bounds].size.height) / 2);
	[self.handleImageView setFrame:handleImageFrame];
	
	if ([self.delegate respondsToSelector:@selector(analogueStickDidChangeValue:)])
	{
		[self.delegate analogueStickDidChangeValue:self];
	}
}

@end
