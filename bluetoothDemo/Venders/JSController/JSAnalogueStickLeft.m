//
//  JSAnalogueStick.m
//  Controller
//
//  Created by James Addyman on 29/03/2013.
//  Copyright (c) 2013 James Addyman. All rights reserved.
//

#import "JSAnalogueStickLeft.h"

#define RADIUS ([self bounds].size.width / 2)

@implementation JSAnalogueStickLeft

- (void)newInit
{
    self.xValue = -1.0f;
    
    CGRect handleImageFrame = [self.handleImageView frame];
    handleImageFrame.origin = CGPointMake(([self bounds].size.width - [self.handleImageView bounds].size.width * 2),
                                          ([self bounds].size.height - [self.handleImageView bounds].size.height) / 2);
    [self.handleImageView setFrame:handleImageFrame];
    
    if ([self.delegate respondsToSelector:@selector(analogueStickDidChangeValue:)])
    {
        [self.delegate analogueStickDidChangeValue:self];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    self.yValue = 0.0f;
    
    CGRect handleImageFrame = [self.handleImageView frame];
    handleImageFrame.origin = CGPointMake(handleImageFrame.origin.x,
                                          ([self bounds].size.height - [self.handleImageView bounds].size.height) / 2);
    [self.handleImageView setFrame:handleImageFrame];
    
    if ([self.delegate respondsToSelector:@selector(analogueStickDidChangeValue:)])
    {
        [self.delegate analogueStickDidChangeValue:self];
    }
}

@end
