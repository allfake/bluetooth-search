//
//  JSAnalogueStick.h
//  Controller
//
//  Created by James Addyman on 29/03/2013.
//  Copyright (c) 2013 James Addyman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSAnalogueStick.h"

@interface JSAnalogueStickLeft : JSAnalogueStick

- (void)newInit;

@end
