//
//  ALFGamepadViewController.m
//  bluetoothDemo
//
//  Created by allfake on 8/23/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFGamepadViewController.h"

@interface ALFGamepadViewController ()

@end

@interface UIDevice (MyPrivateNameThatAppleWouldNeverUseGoesHere)
- (void) setOrientation:(UIInterfaceOrientation)orientation;
@end

@implementation ALFGamepadViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    padModel = [[ALFPadModel alloc] init];
    [padModel loadPad];
    
    [self loadPadData];
    
    self.upButton.delegate = self;
    self.downButton.delegate = self;
    self.leftButton.delegate = self;
    self.rightButton.delegate = self;
    self.aButton.delegate = self;
    self.bButton.delegate = self;
    self.cButton.delegate = self;
    self.dButton.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    [self.peripheral setDelegate:self];
    [self.peripheral setNotifyValue:YES forCharacteristic:self.characteristic];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.manager setDelegate:nil];
}

- (void)loadPadData
{
    self.upButton.dataString = padModel.upButtonString;
    self.downButton.dataString = padModel.downButtonString;
    self.leftButton.dataString = padModel.leftButtonString;
    self.rightButton.dataString = padModel.rightButtonString;
    self.aButton.dataString = padModel.aButtonString;
    self.bButton.dataString = padModel.bButtonString;
    self.cButton.dataString = padModel.cButtonString;
    self.dButton.dataString = padModel.dButtonString;
}

- (IBAction)clickButton:(id)sender
{
    
    NSString *v = ((UIButton *)sender).titleLabel.text;
    
    NSData* valData = [v dataUsingEncoding:NSASCIIStringEncoding];
    
    [self.peripheral writeValue:valData forCharacteristic:self.characteristic type:CBCharacteristicWriteWithoutResponse];
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)buttonDidMove:(ALFPadButton *)button;
{
    
    NSString *v = button.dataString;
    
    NSData* valData = [v dataUsingEncoding:NSASCIIStringEncoding];
    
    CBCharacteristicWriteType type;
    
    if ((self.characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) == CBCharacteristicPropertyWriteWithoutResponse) {
        
        type = CBCharacteristicWriteWithoutResponse;
        
    } else {
        
        type = CBCharacteristicWriteWithResponse;
        
    }
    
    [self.peripheral writeValue:valData forCharacteristic:self.characteristic type:type];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
