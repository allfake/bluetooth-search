//
//  ALFSimpleInputViewController.m
//  bluetoothDemo
//
//  Created by allfake on 8/24/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFSimpleInputViewController.h"

@interface ALFSimpleInputViewController ()

@end

@implementation ALFSimpleInputViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    input = [[ALFSimpleInputModel alloc] init];
    [input loadInput];
    
    self.inputTextFiled.text = input.simpleInputString;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.inputTextFiled becomeFirstResponder];
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    [self.peripheral setDelegate:self];
    [self.peripheral setNotifyValue:YES forCharacteristic:self.characteristic];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.manager setDelegate:nil];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    
}

- (IBAction)sendText:(id)sender
{
    NSString *v = self.inputTextFiled.text;
    NSData* valData = [v dataUsingEncoding:NSUTF8StringEncoding];
    
    CBCharacteristicWriteType type;
    
    if ((self.characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) == CBCharacteristicPropertyWriteWithoutResponse) {
        
        type = CBCharacteristicWriteWithoutResponse;
        
    } else {
        
        type = CBCharacteristicWriteWithResponse;
        
    }
    
    [self.peripheral writeValue:valData forCharacteristic:self.characteristic type:type];
}

- (IBAction)save:(id)sender
{
    input.simpleInputString = self.inputTextFiled.text;
    [input saveInput];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
