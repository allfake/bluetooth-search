//
//  ALFSimpleInputModel.h
//  bluetoothDemo
//
//  Created by allfake on 8/24/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALFSimpleInputModel : NSObject

@property (nonatomic, strong) NSString *simpleInputString;

- (void)saveInput;
- (void)loadInput;

@end
