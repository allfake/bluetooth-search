//
//  ALFPadModel.h
//  bluetoothDemo
//
//  Created by allfake on 8/23/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALFPadModel : NSObject

@property (nonatomic, strong) NSString *upButtonString;
@property (nonatomic, strong) NSString *downButtonString;
@property (nonatomic, strong) NSString *leftButtonString;
@property (nonatomic, strong) NSString *rightButtonString;

@property (nonatomic, strong) NSString *aButtonString;
@property (nonatomic, strong) NSString *bButtonString;
@property (nonatomic, strong) NSString *cButtonString;
@property (nonatomic, strong) NSString *dButtonString;

- (void)savePad;
- (void)loadPad;

@end
