//
//  ALFPadModel.m
//  bluetoothDemo
//
//  Created by allfake on 8/23/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFPadModel.h"
#define UP_BUTTON_STRING @"upButtonString"
#define DOWN_BUTTON_STRING @"downButtonString"
#define LEFT_BUTTON_STRING @"leftButtonString"
#define RIGHT_BUTTON_STRING @"rightButtonString"
#define A_BUTTON_STRING @"aButtonString"
#define B_BUTTON_STRING @"bButtonString"
#define C_BUTTON_STRING @"cButtonString"
#define D_BUTTON_STRING @"dButtonString"

@implementation ALFPadModel

- (void)savePad
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:_upButtonString forKey:UP_BUTTON_STRING];
    [userDefaults setObject:_downButtonString forKey:DOWN_BUTTON_STRING];
    [userDefaults setObject:_leftButtonString forKey:LEFT_BUTTON_STRING];
    [userDefaults setObject:_rightButtonString forKey:RIGHT_BUTTON_STRING];
    [userDefaults setObject:_aButtonString forKey:A_BUTTON_STRING];
    [userDefaults setObject:_bButtonString forKey:B_BUTTON_STRING];
    [userDefaults setObject:_cButtonString forKey:C_BUTTON_STRING];
    [userDefaults setObject:_downButtonString forKey:D_BUTTON_STRING];
    
    [userDefaults synchronize];
}

- (void)loadPad
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    _upButtonString = [userDefaults objectForKey:UP_BUTTON_STRING];
    _downButtonString = [userDefaults objectForKey:DOWN_BUTTON_STRING];
    _leftButtonString = [userDefaults objectForKey:LEFT_BUTTON_STRING];
    _rightButtonString = [userDefaults objectForKey:RIGHT_BUTTON_STRING];
    _aButtonString = [userDefaults objectForKey:A_BUTTON_STRING];
    _bButtonString = [userDefaults objectForKey:B_BUTTON_STRING];
    _cButtonString = [userDefaults objectForKey:C_BUTTON_STRING];
    _downButtonString = [userDefaults objectForKey:D_BUTTON_STRING];
    
    if (_upButtonString == nil) {
        _upButtonString = @"w";
    }
    
    if (_downButtonString == nil) {
        _downButtonString = @"s";
    }
    
    if (_leftButtonString == nil) {
        _leftButtonString = @"a";
    }
    
    if (_rightButtonString == nil) {
        _rightButtonString = @"d";
    }
    
    if (_aButtonString == nil) {
        _aButtonString = @"1";
    }
    
    if (_bButtonString == nil) {
        _bButtonString = @"2";
    }
    
    if (_cButtonString == nil) {
        _cButtonString = @"3";
    }
    
    if (_dButtonString == nil) {
        _dButtonString = @"4";
    }
    
    [self savePad];
    
}

@end
