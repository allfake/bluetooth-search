//
//  ALFSimpleInputModel.m
//  bluetoothDemo
//
//  Created by allfake on 8/24/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFSimpleInputModel.h"
#define SIMPLE_INPUT_STRING @"simpleInputString"

@implementation ALFSimpleInputModel

- (void)saveInput
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:_simpleInputString forKey:SIMPLE_INPUT_STRING];
    
    [userDefaults synchronize];
}

- (void)loadInput
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    _simpleInputString = [userDefaults objectForKey:SIMPLE_INPUT_STRING];
    
    if (_simpleInputString == nil) {
        _simpleInputString = @"Hello Ble";
    }
    
    [self saveInput];
    
}


@end
