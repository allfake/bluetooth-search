//
//  ALFPadButton.m
//  bluetoothDemo
//
//  Created by allfake on 8/23/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFPadButton.h"

@implementation ALFPadButton
{
    NSTimer *timer;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)moving:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(buttonDidMove:)]) {
        [self.delegate buttonDidMove:self];
    }
    
}

- (void)touchesBegan:(NSSet*)touches  withEvent:(UIEvent*)event {
    [super touchesBegan:touches withEvent:event];
    
    [self moving:nil];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(moving:) userInfo:nil repeats:YES];
}

- (void)touchesEnded:(NSSet*)touches  withEvent:(UIEvent*)event {
    [super touchesEnded:touches withEvent:event];
    if (timer != nil)
        [timer invalidate];
    timer = nil;
}

- (void)touchesMoved:(NSSet*)touches  withEvent:(UIEvent*)event {
    [super touchesMoved:touches withEvent:event];
    if (timer != nil) {
//        [timer invalidate];
//        timer = nil;
    }
}

@end
