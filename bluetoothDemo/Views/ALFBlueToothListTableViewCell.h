//
//  ALFBlueToothListTableViewCell.h
//  bluetoothDemo
//
//  Created by allfake on 6/29/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const ALFBlueToothListTableViewCellCellIdentifier = @"ALFBlueToothListTableViewCellIdentifier";

@interface ALFBlueToothListTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *blueToothTitle;
@property (nonatomic, strong) IBOutlet UILabel *blueToothType;
@property (nonatomic, strong) IBOutlet UILabel *blueToothIdentifier;

@end
