//
//  ALFPadButton.h
//  bluetoothDemo
//
//  Created by allfake on 8/23/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ALFPadButtonDelegate;

@interface ALFPadButton : UIButton

@property (nonatomic, strong) NSString *dataString;
@property (nonatomic, weak) id<ALFPadButtonDelegate> delegate;

@end


@protocol ALFPadButtonDelegate <NSObject>
@optional

- (void)buttonDidMove:(UIButton *)button;

@end
