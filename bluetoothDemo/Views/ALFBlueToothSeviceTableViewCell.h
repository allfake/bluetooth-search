//
//  ALFBlueToothSeviceTableViewCell.h
//  bluetoothDemo
//
//  Created by allfake on 7/6/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const ALFBlueToothSeviceTableViewCellIdentifier = @"ALFBlueToothSeviceTableViewCellIdentifier";

@interface ALFBlueToothSeviceTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *title;

@end
