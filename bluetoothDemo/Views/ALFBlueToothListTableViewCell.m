//
//  ALFBlueToothListTableViewCell.m
//  bluetoothDemo
//
//  Created by allfake on 6/29/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFBlueToothListTableViewCell.h"

@implementation ALFBlueToothListTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)copyIndentifier:(id)sender
{
    [[UIPasteboard generalPasteboard] setString:self.blueToothIdentifier.text];
}

@end
