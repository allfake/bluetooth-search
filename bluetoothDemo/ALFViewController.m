//
//  ALFViewController.m
//  bluetoothDemo
//
//  Created by allfake on 6/14/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFViewController.h"
#import "ALFBlueToothListTableViewCell.h"
#import "ALFBluetoothDetailViewController.h"

@interface ALFViewController ()

@end

@implementation ALFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], CBCentralManagerScanOptionAllowDuplicatesKey, nil];

    bluetoothArray = [NSMutableArray new];
    
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    self.manager.delegate = nil;
//    self.manager = nil;
    
}

- (IBAction)reScan:(id)sender {
    bluetoothArray = [NSMutableArray new];
    [self.manager scanForPeripheralsWithServices:nil options:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if ([central state] == CBCentralManagerStatePoweredOn) {
        [self reScan:nil];
        self.statusLabel.text = @"";
        
    } else if ([central state] == CBCentralManagerStateUnsupported) {
        
        self.statusLabel.text = @"● You device not support \n\n(╥﹏╥)";
        
    } else if ([central state] == CBCentralManagerStateUnauthorized) {
        
        self.statusLabel.text = @"● Please authorized to use bluetooth \n\n(╥﹏╥)";
        
    } else if ([central state] == CBCentralManagerStatePoweredOff) {
        
        self.statusLabel.text = @"● Bluetooth power off \n\n(╥﹏╥)";
        
    } else {
        
//        self.statusLabel.text = @"● Not connect";
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    for (CBPeripheral *peripheral in peripherals) {
        NSLog(@"peripheral name %@", peripheral.name);
//        NSLog(@"peripheral name %ld", peripheral.UUID);

        [self.manager connectPeripheral:peripheral options:nil];
        
//        [self.manager connectPeripheral:peripheral options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBConnectPeripheralOptionNotifyOnNotificationKey]];
    }
}

- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (![bluetoothArray containsObject:aPeripheral.identifier.UUIDString]) {
        [bluetoothArray addObject:aPeripheral];
    }
//    if ([aPeripheral.name isEqualToString:@"HMSoft"]) {
//        [self.manager retrievePeripherals:[NSArray arrayWithObject:aPeripheral.UUID]];
//    }
    
    [self.tableView reloadData];
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    [hud hide:YES];
//    [aPeripheral setDelegate:self];
//    [aPeripheral discoverServices:nil];
    
    [self performSegueWithIdentifier:@"bluetoothSegueIndentifier" sender:aPeripheral];

}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    hud.labelText = @"Connect failed";
    [hud hide:YES afterDelay:2.0f];
}


- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error
{
    for (CBService *aService in aPeripheral.services)
    {
        NSLog(@"Service found with UUID: %@", aService.UUID);
        
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"FFE0"]])
        {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
        
    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"FFE0"]])
    {
        for (CBCharacteristic *aChar in service.characteristics)
        {
            NSLog(@"Characteristic %@", aChar.UUID);
            
            /* Read body sensor location */
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"FFE1"]])
            {
                [aPeripheral setNotifyValue:YES forCharacteristic:aChar];


//                [self.manager connectPeripheral:aPeripheral options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBConnectPeripheralOptionNotifyOnConnectionKey]];
                
                
            }
            
        }
    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"FFE1"]]) {
        
        if (characteristic.value || !error) {
            NSString *myString = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];

            NSLog(@"characteristic.value %@ na", myString);

            NSString* v = @"111";
            
            NSData* valData = [v dataUsingEncoding:NSUTF16StringEncoding];
            
            [aPeripheral writeValue:valData forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            
        }

    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
//    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"FFE1"]]) {
    NSLog(@"didWriteValueForCharacteristic %@", error);
        if (characteristic.value || !error) {
            NSString *myString = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
            
            NSLog(@"didWriteValueForCharacteristic %@", myString);
            
            
        }
        
//    }
}

#pragma mark - Tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [bluetoothArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [tableView dequeueReusableCellWithIdentifier:ALFBlueToothListTableViewCellCellIdentifier];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ALFBlueToothListTableViewCell class])
                                              owner:self
                                            options:nil] lastObject];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CBPeripheral *peripheral = [bluetoothArray objectAtIndex:indexPath.row];
    
    ALFBlueToothListTableViewCell *aCell = (ALFBlueToothListTableViewCell *)cell;
    
    if (!peripheral.name || [peripheral.name isEqualToString:@""]) {
        
        aCell.blueToothTitle.text = @"Unnamed";
        
    } else {
        
        aCell.blueToothTitle.text = peripheral.name;
        
    }
    
    aCell.blueToothIdentifier.text = peripheral.identifier.UUIDString;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBPeripheral *peripheral = [bluetoothArray objectAtIndex:indexPath.row];
    [self.manager retrievePeripherals:[NSArray arrayWithObject:peripheral.UUID]];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
}

#pragma mark - navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"bluetoothSegueIndentifier"]) {
     
        [self.manager stopScan];
//        [self.manager setDelegate:nil];
        
        ALFBluetoothDetailViewController *destinationViewController = (ALFBluetoothDetailViewController *)[segue destinationViewController];
        [destinationViewController setPeripheral:sender];

    }
}

@end
