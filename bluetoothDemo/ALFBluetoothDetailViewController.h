//
//  ALFBluetoothDetailViewController.h
//  bluetoothDemo
//
//  Created by allfake on 7/6/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ALFBluetoothDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CBPeripheralDelegate, CBCentralManagerDelegate>
{
    NSMutableDictionary *dataSource;
}
@property (readwrite, nonatomic) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UILabel *blueToothStatus;
@property (nonatomic, strong) IBOutlet UILabel *blueToothUUID;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@end
