//
//  ALFVehicleController.m
//  BLE Controller
//
//  Created by allfake on 11/9/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFVehicleViewController.h"

@implementation ALFVehicleViewController
{
    NSTimer *timer;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    [self.peripheral setDelegate:self];
    [self.peripheral setNotifyValue:YES forCharacteristic:self.characteristic];
    
    [self.analogueLeftStick newInit];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.05f
                                     target:self
                                   selector:@selector(sendData)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.manager setDelegate:nil];
    [timer invalidate];
    timer = nil;
}

- (void)analogueStickDidChangeValue:(JSAnalogueStick *)analogueStick
{

}

- (void)sendData
{
    
    int8_t startVal = 125;
    
    int8_t xLeftValue = self.analogueLeftStick.xValue * 120.f;
    int8_t yLeftValue = self.analogueLeftStick.yValue * 120.f;
    
    int8_t xRightValue = self.analogueRightStick.xValue * 120.f;
    int8_t yRightValue = self.analogueRightStick.yValue * 120.f;
    
    NSMutableData *valData = [[NSMutableData alloc] init];
    
    [valData appendBytes:&startVal length:sizeof(startVal)];
    [valData appendBytes:&yRightValue length:sizeof(yRightValue)];
    [valData appendBytes:&xRightValue length:sizeof(xRightValue)];
    [valData appendBytes:&xLeftValue length:sizeof(xLeftValue)];
    [valData appendBytes:&yLeftValue length:sizeof(yLeftValue)];
    
    [valData appendBytes:&startVal length:sizeof(startVal)];
    [valData appendBytes:&yRightValue length:sizeof(yRightValue)];
    [valData appendBytes:&xRightValue length:sizeof(xRightValue)];
    [valData appendBytes:&xLeftValue length:sizeof(xLeftValue)];
    [valData appendBytes:&yLeftValue length:sizeof(yLeftValue)];
    
    CBCharacteristicWriteType type;
    
    if ((self.characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) == CBCharacteristicPropertyWriteWithoutResponse) {
        
        type = CBCharacteristicWriteWithoutResponse;
        
    } else {
        
        type = CBCharacteristicWriteWithResponse;
        
    }
    
    [self.peripheral writeValue:valData forCharacteristic:self.characteristic type:type];
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state != CBCentralManagerStatePoweredOn) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
