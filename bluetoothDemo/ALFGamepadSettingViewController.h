//
//  ALFPadViewController.h
//  bluetoothDemo
//
//  Created by allfake on 7/12/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "ALFPadModel.h"

@interface ALFGamepadSettingViewController : UIViewController
{
    ALFPadModel *padModel;
}
@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) IBOutlet UITextField *upButton;
@property (nonatomic, strong) IBOutlet UITextField *downButton;
@property (nonatomic, strong) IBOutlet UITextField *leftButton;
@property (nonatomic, strong) IBOutlet UITextField *rightButton;
@property (nonatomic, strong) IBOutlet UITextField *aButton;
@property (nonatomic, strong) IBOutlet UITextField *bButton;
@property (nonatomic, strong) IBOutlet UITextField *cButton;
@property (nonatomic, strong) IBOutlet UITextField *dButton;

@end
