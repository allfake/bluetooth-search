//
//  ALFGamepadViewController.h
//  bluetoothDemo
//
//  Created by allfake on 8/23/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "ALFPadModel.h"
#import "ALFPadButton.h"

@interface ALFGamepadViewController : UIViewController <CBPeripheralDelegate, CBCentralManagerDelegate, ALFPadButtonDelegate>
{
    ALFPadModel *padModel;
}

@property (readwrite, nonatomic) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) CBCharacteristic *characteristic;

@property (nonatomic, strong) IBOutlet ALFPadButton *upButton;
@property (nonatomic, strong) IBOutlet ALFPadButton *downButton;
@property (nonatomic, strong) IBOutlet ALFPadButton *leftButton;
@property (nonatomic, strong) IBOutlet ALFPadButton *rightButton;
@property (nonatomic, strong) IBOutlet ALFPadButton *aButton;
@property (nonatomic, strong) IBOutlet ALFPadButton *bButton;
@property (nonatomic, strong) IBOutlet ALFPadButton *cButton;
@property (nonatomic, strong) IBOutlet ALFPadButton *dButton;

@end
