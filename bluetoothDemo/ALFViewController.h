//
//  ALFViewController.h
//  bluetoothDemo
//
//  Created by allfake on 6/14/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface ALFViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CBPeripheralDelegate, CBCentralManagerDelegate>
{
    NSMutableArray *bluetoothArray;
    MBProgressHUD *hud;
}

@property (nonatomic, strong) IBOutlet UILabel *statusLabel;
@property (readwrite, nonatomic) CBCentralManager *manager;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
