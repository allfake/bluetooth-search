//
//  ALFAppDelegate.h
//  bluetoothDemo
//
//  Created by allfake on 6/14/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
