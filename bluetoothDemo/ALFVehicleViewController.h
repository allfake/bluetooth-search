//
//  ALFVehicleController.h
//  BLE Controller
//
//  Created by allfake on 11/9/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "JSAnalogueStick.h"
#import "JSAnalogueStickRight.h"
#import "JSAnalogueStickLeft.h"

@interface ALFVehicleViewController : UIViewController <CBPeripheralDelegate, CBCentralManagerDelegate, JSAnalogueStickDelegate>

@property (readwrite, nonatomic) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) CBCharacteristic *characteristic;

@property (weak, nonatomic) IBOutlet JSAnalogueStickLeft *analogueLeftStick;
@property (weak, nonatomic) IBOutlet JSAnalogueStickRight *analogueRightStick;


@end
