//
//  ALFCharacteristicViewController.m
//  bluetoothDemo
//
//  Created by allfake on 7/6/14.
//  Copyright (c) 2014 allfake. All rights reserved.
//

#import "ALFCharacteristicViewController.h"
#import "ALFGamepadViewController.h"
#import "ALFColorPickerViewController.h"
#import "ALFSimpleInputViewController.h"
#import "ALFVehicleViewController.h"

FOUNDATION_STATIC_INLINE CGRect CGRectMakeWithSize(CGFloat x, CGFloat y, CGSize size){
	CGRect r; r.origin.x = x; r.origin.y = y; r.size = size; return r;
}

@interface ALFCharacteristicViewController ()

@end

@implementation ALFCharacteristicViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.characteristic.UUID.UUIDString;
    
    dataOption = 0;
	self.multiswitch = [[TKMultiSwitch alloc] initWithItems:@[@"Raw data", @"String"]];
	self.multiswitch.frame = CGRectMakeWithSize(self.multiswitchView.bounds.origin.x, self.multiswitchView.bounds.origin.y, self.multiswitchView.bounds.size);
	[self.multiswitch addTarget:self action:@selector(changedSwitchValue:) forControlEvents:UIControlEventValueChanged];
	[self.multiswitchView addSubview:self.multiswitch];

    input = [[ALFSimpleInputModel alloc] init];
    [input loadInput];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    [self.peripheral setDelegate:self];
    [self.peripheral setNotifyValue:YES forCharacteristic:self.characteristic];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.manager setDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.peripheral setDelegate:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"gamepadIndentifier"]) {
        ALFGamepadViewController *vc = [segue destinationViewController];
        
        vc.peripheral = self.peripheral;
        vc.characteristic = self.characteristic;
    } else if ([segue.identifier isEqualToString:@"colorPickerIndentifier"]) {

        ALFColorPickerViewController *vc = [segue destinationViewController];
        
        vc.peripheral = self.peripheral;
        vc.characteristic = self.characteristic;
    } else if ([segue.identifier isEqualToString:@"simpleInputIndentifier"]) {
        
        ALFSimpleInputViewController *vc = [segue destinationViewController];
        
        vc.peripheral = self.peripheral;
        vc.characteristic = self.characteristic;
    } else if ([segue.identifier isEqualToString:@"vehicleIndentifier"]) {
        
        ALFVehicleViewController *vc = [segue destinationViewController];
        
        vc.peripheral = self.peripheral;
        vc.characteristic = self.characteristic;
    }
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    
}

- (void) peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (characteristic.value || !error) {
        if (dataOption == 0) {
            
            self.readDataLabel.text = [NSString stringWithFormat:@"%@", characteristic.value];
            
        } else if (dataOption == 1) {
            
            NSString *myString = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];

            self.readDataLabel.text = myString;

        }
        
    }
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self performSegueWithIdentifier:@"gamepadSettingIndentifier" sender:nil];
                    break;
                default:
                    break;
            }
            break;
        }
        case 2: {
            switch (buttonIndex) {
                case 0:
                    [self performSegueWithIdentifier:@"simpleInputIndentifier" sender:nil];
                    break;
                case 1:
                    [self performSegueWithIdentifier:@"gamepadIndentifier" sender:nil];
                    break;
                case 2:
                    [self performSegueWithIdentifier:@"colorPickerIndentifier" sender:nil];
                    break;
                case 3:
                    [self performSegueWithIdentifier:@"vehicleIndentifier" sender:nil];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    
    
}

- (IBAction)setting:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select setting for controller" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Joystick",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
}

- (IBAction)selectedController:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select controller" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Simple input",
                            @"Joystick",
                            @"Color picker",
                            @"Vehicle controller",
                            nil];
    popup.tag = 2;
    [popup showInView:self.view];
}

- (void) changedSwitchValue:(TKMultiSwitch*)switcher{
    dataOption = switcher.indexOfSelectedItem;
}

- (IBAction)sendSimpleInput:(id)sender
{
    
    NSString *v = input.simpleInputString;
    
    NSData* valData = [v dataUsingEncoding:NSASCIIStringEncoding];
    
    [self.peripheral writeValue:valData forCharacteristic:self.characteristic type:CBCharacteristicWriteWithoutResponse];
}

@end
