
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Color-Picker-for-iOS
#define COCOAPODS_POD_AVAILABLE_Color_Picker_for_iOS
#define COCOAPODS_VERSION_MAJOR_Color_Picker_for_iOS 2
#define COCOAPODS_VERSION_MINOR_Color_Picker_for_iOS 0
#define COCOAPODS_VERSION_PATCH_Color_Picker_for_iOS 0

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 8
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 0

// TPKeyboardAvoiding
#define COCOAPODS_POD_AVAILABLE_TPKeyboardAvoiding
#define COCOAPODS_VERSION_MAJOR_TPKeyboardAvoiding 1
#define COCOAPODS_VERSION_MINOR_TPKeyboardAvoiding 2
#define COCOAPODS_VERSION_PATCH_TPKeyboardAvoiding 3

// TapkuLibrary
#define COCOAPODS_POD_AVAILABLE_TapkuLibrary
#define COCOAPODS_VERSION_MAJOR_TapkuLibrary 0
#define COCOAPODS_VERSION_MINOR_TapkuLibrary 3
#define COCOAPODS_VERSION_PATCH_TapkuLibrary 3

